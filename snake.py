# -*- coding: utf-8 -*-
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************          importation des modules             ************************* *#
#****************************************************************************************************#
import tkinter
from tkinter import messagebox
from game import *
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************      configuration des touches du jeu        ************************* *#
#****************************************************************************************************#
def screenCenter():
    app = tkinter.Tk()
    app["bg"] = "#87CEEB"
    app.title("Snake ~ Assistance")
    app.resizable(width = False, height = False)
    screensX = int(app.winfo_screenwidth())
    screensY = int(app.winfo_screenheight())
    windX = (screensX * 40) // 100
    windY = (screensY * 40) // 100
    pX = (screensX // 2) - (windX // 2)
    pY = (screensY // 2) - (windY // 2)
    geo = "{}x{}+{}+{}".format(windX, windY, pX, pY)
    app.geometry(geo)
    MedSize = int((float(screensY) * 1.6) / 100)
    SmallSize = int((float(screensY) * 1.3) / 100)
    TitlePosX = (windX * 10) // 100
    TitlePosY = (windY * 5) // 100
    Title = tkinter.Label(app, text = "Game Assistance", font = ("Castellar", MedSize, "normal"))
    Title["bg"] = "#87CEEB"
    Title.place(x = TitlePosX, y = TitlePosY)
    AsidePosX = (windX * 5) // 100
    AsidePosY = (windY * 13) // 100
    Aside = tkinter.Label(app, text = "~ This  game is that of famous snake that eats  apples\n~ Choose  the standard configuration to play with the\ndirection  keys  of  your  defaut  keyboard\n~ Make  a   manual   configuration   of   your   choice\n~ Attention,   the  manual   configuration  only  takes\ninto account the alphanumerics key\n~ Pay  attention  to  the  manual  configuration  so  not\nto have errors\n~ Click     on     a    difficulty    level     to     select    it", font = ("Gabriola", SmallSize, "normal"))
    Aside["bg"] = "#87CEEB"
    Aside.place(x = AsidePosX, y = AsidePosY)
    BtnExitPosx = (windX * 45) // 100
    BtnExitPosY = (windY * 85) // 100
    BtnExit = tkinter.Button(app, text = "Exit", font = ("algerian", SmallSize, "normal"), borderwidth = 5, width = 8, height = 1, command = app.quit)
    BtnExit['bg'] = "#FF2C15"
    BtnExit.place(x = BtnExitPosx, y = BtnExitPosY)
    app.mainloop()
def getEntryRadio(*args):
    pass
def showWindows(args):
    if args == "error":
        messagebox.showerror("Error", "Configured Error detected !")
    elif args == "alert":
        messagebox.showwarning("Alert", "Pay attention to The commands you enter !")
    elif args == "info":
        messagebox.showinfo("Info", "~ Version: 1.0.0 (user setup)\n\n~ Date: 2022-09-16T08:36:10.600Z\n\n~ OS: Windows_NT x64 - Windows_NT x32")
def level():
    level = VarGetRadio.get()
    if level == 0:
        return 0.2
    elif level == 1:
        return 0.11
    elif level == 2:
        return 0.07
def getStandard():
    btn = ["z", "s", "q", "d"]
    times = level()
    return snake(btn, times = times)
def getData():
    btn = [VarEntryUp.get(), VarEntryDown.get(), VarEntryLeft.get(), VarEntryRight.get()]
    if len(btn[0]) != 1 or len(btn[1]) != 1 or len(btn[2]) != 1 or len(btn[3]) != 1:
        return showWindows(args = "alert")
    elif ((btn[0].isalpha() or btn[0].isdigit()) and (btn[1].isalpha() or btn[1].isdigit()) and (btn[2].isalpha() or btn[2].isdigit()) and (btn[3].isalpha() or btn[3].isdigit())) and ((btn.count(btn[0]) == 1) and (btn.count(btn[1]) == 1) and (btn.count(btn[2]) == 1) and (btn.count(btn[3]) == 1)):
        times = level()
        return snake(btn = btn, times = times)
    else:
        return showWindows(args = "error")
def aboutCommand():
    return showWindows(args = "info")
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************    creation de la fenetre de configuration     *********************** *#
#****************************************************************************************************#
apps = tkinter.Tk()
apps['bg'] = "#4A5A6A"
apps.title("Snake ~ Setup")
apps.resizable(width = False, height = False)
screenX = int(apps.winfo_screenwidth())
screenY = int(apps.winfo_screenheight())
windowsX = (screenX * 45) // 100
windowsY = (screenY * 45) // 100
posX = (screenX // 2) - (windowsX // 2)
posY = (screenY // 2) - (windowsY // 2)
geometry = "{}x{}+{}+{}".format(windowsX, windowsY, posX, posY)
apps.geometry(geometry)
LabePosX = (windowsX * 25) // 100
EntryPosX = (windowsX * 65) // 100
RadioPosY = (windowsY * 72) // 100
ButtonPosY = (windowsY * 85) // 100
BigSize = int((float(screenY) * 2.6) / 100)
MedSize = int((float(screenY) * 1.9) / 100)
SmallSize = int((float(screenY) * 1.5) / 100)
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                    Menu                      ************************* *#
#****************************************************************************************************#
AppMenu = tkinter.Menu(apps)
Helps = tkinter.Menu(AppMenu, tearoff = 0)
Helps["bg"] = "#87CEEB"
Helps.add_command(label = "Assistance", font = "Cambria 12", command = screenCenter)
Helps.add_command(label = "About", font = "Cambria 12", command = aboutCommand)
Credits = tkinter.Menu(AppMenu, tearoff = 0, font = "Cambria 12")
Credits["bg"] = "#87CEEB"
Credits.add_command(label = "Credits", font = "Cambria 12")
AppMenu.add_cascade(label = "Helps", menu = Helps)
AppMenu.add_cascade(label = "Credits", menu = Credits)
apps.config(menu = AppMenu)
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                    TitleKey                  ************************* *#
#****************************************************************************************************#
TitleKeySetupPosX = (windowsX * 21) // 100
TitleKeySetupPosY = (windowsY * 3) // 100
TitleKeySetup = tkinter.Label(apps, text = "KEY CONFIGURATION", font = ("Castellar", BigSize, "normal"))
TitleKeySetup.place(x = TitleKeySetupPosX, y = TitleKeySetupPosY)
TitleKeySetup['bg'] = "lightgray"
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                   Config Up                   ************************ *#
#****************************************************************************************************#
UpPosY = (windowsY * 15) // 100
LabelUp = tkinter.Label(apps, text = "=> Up      ......................................", font = ("Gabriola", MedSize, "bold"))
LabelUp.place(x = LabePosX, y = UpPosY)
LabelUp['bg'] = "#4A5A6A"
VarEntryUp = tkinter.StringVar()
VarEntryUp.trace("r", getEntryRadio)
EntryUp = tkinter.Entry(apps, textvariable = VarEntryUp, font = ("Cambria", SmallSize, "bold"), width = 2, borderwidth = 3)
EntryUp['bg'] = "#87CEEB"
EntryUp.place(x = EntryPosX, y = UpPosY)
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                  Config Down                  ************************ *#
#****************************************************************************************************#
DownPosY = (windowsY * 25) // 100
LabelDown = tkinter.Label(apps, text = "=> Down  ......................................", font = ("Gabriola", MedSize, "bold"))
LabelDown.place(x = LabePosX, y = DownPosY)
LabelDown['bg'] = "#4A5A6A"
VarEntryDown = tkinter.StringVar()
VarEntryDown.trace("r", getEntryRadio)
EntryDown = tkinter.Entry(apps, textvariable = VarEntryDown, font = ("Cambria", SmallSize, "bold"), width = 2, borderwidth = 3)
EntryDown['bg'] = "#87CEEB"
EntryDown.place(x = EntryPosX, y = DownPosY)
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                  Config Left                  ************************ *#
#****************************************************************************************************#
LeftPosY = (windowsY * 35) // 100
LabelLeft = tkinter.Label(apps, text = "=> Left     ......................................", font = ("Gabriola", MedSize, "bold"))
LabelLeft.place(x = LabePosX, y = LeftPosY)
LabelLeft['bg'] = "#4A5A6A"
VarEntryLeft = tkinter.StringVar()
VarEntryLeft.trace("r", getEntryRadio)
EntryLeft = tkinter.Entry(apps, textvariable = VarEntryLeft, font = ("Cambria", SmallSize, "bold"), width = 2, borderwidth = 3)
EntryLeft['bg'] = "#87CEEB"
EntryLeft.place(x = EntryPosX, y = LeftPosY)
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                  Config Right                 ************************ *#
#****************************************************************************************************#
RightPosY = (windowsY * 45) // 100
LabelRight = tkinter.Label(apps, text = "=> Right  .......................................", font = ("Gabriola", MedSize, "bold"))
LabelRight.place(x = LabePosX, y = RightPosY)
LabelRight['bg'] = "#4A5A6A"
VarEntryRight = tkinter.StringVar()
VarEntryRight.trace("r", getEntryRadio)
EntryRight = tkinter.Entry(apps, textvariable = VarEntryRight, font = ("Cambria", SmallSize, "bold"), width = 2, borderwidth = 3)
EntryRight['bg'] = "#87CEEB"
EntryRight.place(x = EntryPosX, y = RightPosY)
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                   TitleLevel                 ************************* *#
#****************************************************************************************************#
TitleLevelSetupPosX = (windowsX * 20) // 100
TitleLevelSetupPosY = (windowsY * 60) // 100
TitleLevelSetup = tkinter.Label(apps, text = "LEVEL CONFIGURATION", font = ("Castellar", BigSize, "normal"))
TitleLevelSetup.place(x = TitleLevelSetupPosX, y = TitleLevelSetupPosY)
TitleLevelSetup['bg'] = "lightgray"
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                   CheckLevel                 ************************* *#
#****************************************************************************************************#
VarGetRadio = tkinter.IntVar()
VarGetRadio.trace("w", getEntryRadio)
EasyRadioPosX = (windowsX * 15) // 100
EasyRadio = tkinter.Radiobutton(apps, variable = VarGetRadio, text = "Easy", value = 0, font = ("Cambria", SmallSize, "bold"))
EasyRadio.place(x = EasyRadioPosX, y = RadioPosY)
EasyRadio['bg'] = "#4A5A6A"
MeduimRadioPosX = (windowsX * 45) // 100
MeduimRadio = tkinter.Radiobutton(apps, variable = VarGetRadio, text = "Medium", value = 1, font = ("Cambria", SmallSize, "bold"))
MeduimRadio.place(x = MeduimRadioPosX, y = RadioPosY)
MeduimRadio['bg'] = "#4A5A6A"
HardRadioPosX = (windowsX * 72) // 100
HardRadio = tkinter.Radiobutton(apps, variable = VarGetRadio, text = "Hard", value = 2, font = ("Cambria", SmallSize, "bold"))
HardRadio.place(x = HardRadioPosX, y = RadioPosY)
HardRadio['bg'] = "#4A5A6A"
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                     Button                   ************************* *#
#****************************************************************************************************#
ButtonStandardPosX = (windowsX * 17) // 100
ButtonStandard = tkinter.Button(apps, text = "STANDARD", font = ("algerian", SmallSize, "normal"), borderwidth = 6, width = 8, height = 1, command = getStandard)
ButtonStandard.place(x = ButtonStandardPosX, y = ButtonPosY)
ButtonStandard['bg'] = "#87CEEB"
ButtonConfirmPosX = (windowsX * 43) // 100
ButtonConfirm = tkinter.Button(apps, text = "CONFIRM", font = ("algerian", SmallSize, "normal"), borderwidth = 6, width = 8, height = 1, command = getData)
ButtonConfirm.place(x = ButtonConfirmPosX, y = ButtonPosY)
ButtonConfirm['bg'] = "#32CD32"
ButtonQuitPosX = (windowsX * 69) // 100
ButtonQuit = tkinter.Button(apps, text = "EXIT", font = ("algerian", SmallSize, "normal"), borderwidth = 6, width = 8, height = 1, command = apps.quit)
ButtonQuit.place(x = ButtonQuitPosX, y = ButtonPosY)
ButtonQuit['bg'] = "#FF2C15"

apps.mainloop()
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                   The End                    ************************* *#
#****************************************************************************************************#
#****************************************************************************************************#