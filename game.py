# -*- coding: utf-8 -*-
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************          importation des modules             ************************* *#
#****************************************************************************************************#
import tkinter
import turtle
import random
import time
def snake(btn, times):
    playerScore = 0
    higthsScore = 0
    delayTime = times
    app = tkinter.Tk()
    screenX = int(app.winfo_screenwidth())
    screenY = int(app.winfo_screenheight())
    app.destroy()
    windowsX = (screenX // 2)#630
    windowsY = (screenY // 2)#635
    borderWindonwsX = (windowsX // 2) - 20
    borderWindonwsY = (windowsY // 2) - 20
#****************************************************************************************************#
#* *************************             creation du canevas              ************************* *#
#****************************************************************************************************#
    wind = turtle.Screen()
    wind.clear()
    wind = turtle.Screen()
    wind.title("Snake ~ Game")
    wind.setup(width = windowsX, height = windowsY)
    wind.bgcolor("#87CEEB")
    wind.tracer(0)
#****************************************************************************************************#
#* *************************              creation de snake               ************************* *#
#****************************************************************************************************#
    snake = turtle.Turtle()
    snake.shape("square")
    snake.color("black")
    snake.penup()
    xbeginsnake, ybeginsnake = random.randint(-borderWindonwsX, borderWindonwsX), random.randint(-borderWindonwsY, borderWindonwsY)
    snake.goto(xbeginsnake, ybeginsnake)
    snake.direction = "Stop"
#****************************************************************************************************#
#* *************************              creation de food                ************************* *#
#****************************************************************************************************#
    food = turtle.Turtle()
    food.speed(0)
    food.shape("circle")
    food.color("#FF2C15")
    food.penup()
    xbeginfood, ybeginfood = random.randint(-borderWindonwsX, borderWindonwsX), random.randint(-borderWindonwsY, borderWindonwsY)
    food.goto(xbeginfood, ybeginfood)
#****************************************************************************************************#
#* *************************        constructeur du score du jeu          ************************* *#
#****************************************************************************************************#
    pen = turtle.Turtle()
    pen.speed(0)
    pen.shape("square")
    pen.color("black")
    pen.penup()
    pen.hideturtle()
    pen.goto(0, borderWindonwsY)
    pen.write("Score : 0 Higths Score : 0", align = "center", font = ("Cambria", 15, "bold"))
#****************************************************************************************************#
#* *********************    gestion des touche de direction pour le serpent    ******************** *#
#****************************************************************************************************#
    def ups():
        if snake.direction != "down":
            snake.direction = "up"
    def downs():
        if snake.direction != "up":
            snake.direction = "down"
    def lefts():
        if snake.direction != "right":
            snake.direction = "left"
    def rigths():
        if snake.direction != "left":
            snake.direction = "right"
#****************************************************************************************************#
#* *************************      gestion des mouvements du serpent       ************************* *#
#****************************************************************************************************#
    def move():
        if snake.direction == "up":
            ycoord = snake.ycor()
            snake.sety(ycoord + 20)
        if snake.direction == "down":
            ycoord = snake.ycor()
            snake.sety(ycoord - 20)
        if snake.direction == "left":
            xcoord = snake.xcor()
            snake.setx(xcoord - 20)
        if snake.direction == "right":
            xcoord = snake.xcor()
            snake.setx(xcoord + 20)  
#****************************************************************************************************#
#* *************************      placer les parametres de direction      ************************* *#
#****************************************************************************************************#
    wind.listen()
    wind.onkeypress(ups, btn[0])
    wind.onkeypress(downs, btn[1])
    wind.onkeypress(lefts, btn[2])
    wind.onkeypress(rigths, btn[3])
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************      implementation du gameplay du jeu       ************************* *#
#****************************************************************************************************#
#****************************************************************************************************#
    bodys = []
    while True:
        wind.update()
        if snake.xcor() > borderWindonwsX or snake.xcor() < -borderWindonwsX or snake.ycor() > borderWindonwsY or snake.ycor() < -borderWindonwsY:
            time.sleep(1)
            xcoord, ycoord = random.randint(-borderWindonwsX, borderWindonwsX), random.randint(-borderWindonwsY, borderWindonwsY)
            snake.goto(xcoord, ycoord)
            snake.direction = "Stop"
            snake.shape("square")
            snake.color("black")       
            for body in bodys:
                body.goto(1000, 1000)
            bodys.clear()
            playerScore = 0
            delayTime = times
            pen.clear()
            pen.write("Score : {} Higths Score : {}".format(playerScore, higthsScore), align = "center", font = ("Cambria", 15, "bold"))
        if snake.distance(food) < 20:
            xcoord = random.randint(-borderWindonwsX, borderWindonwsX)
            ycoord = random.randint(-borderWindonwsY, borderWindonwsY)
            food.goto(xcoord, ycoord)
            #* **************************  faire grandire le serpent  ******************************** *#
            addBody = turtle.Turtle()
            addBody.speed(0)
            addBody.shape("square")
            addBody.color("#2A303D")
            addBody.penup()
            bodys.append(addBody)
            playerScore+=1
            t = times*0.01
            delayTime-=t
            if playerScore > higthsScore:
                higthsScore = playerScore
            pen.clear()
            pen.write("Score : {} Higths Score : {}".format(playerScore, higthsScore), align = "center", font = ("Cambria", 15, "bold"))
        #* ***********************************  collision  ******************************************* *#
        for i in range(len(bodys)-1, 0, -1):
            xcoord = bodys[i-1].xcor()
            ycoord = bodys[i-1].ycor()
            bodys[i].goto(xcoord, ycoord) 
        if len(bodys) > 0:
            xcoord = snake.xcor()
            ycoord = snake.ycor()
            bodys[0].goto(xcoord, ycoord)
        #* ************************************  deplacement  **************************************** *#
        move()   
        for body in bodys:
            if body.distance(snake) < 20:
                time.sleep(1)
                xcoord, ycoord = random.randint(-borderWindonwsX, borderWindonwsX), random.randint(-borderWindonwsY, borderWindonwsY)
                snake.goto(xcoord, ycoord)
                snake.direction = "Stop"
                snake.color("black")
                snake.shape("square")
                for body in bodys:
                    body.goto(1000, 1000)
                body.clear()
                playerScore = 0
                delayTime = times
                pen.clear()
                pen.write("Score : {} Higths Score : {}".format(playerScore, higthsScore), align = "center", font = ("Cambria", 15, "bold"))
        time.sleep(delayTime)
    wind.mainloop()
#****************************************************************************************************#
#****************************************************************************************************#
#* *************************                   The End                    ************************* *#
#****************************************************************************************************#
#****************************************************************************************************#